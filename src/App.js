import logo from './logo.svg';
import './App.css';
import GuitarList from './components/GuitarList';

function App() {
  return (
    <div className="App">
     <GuitarList></GuitarList>
    </div>
  );
}

export default App;
