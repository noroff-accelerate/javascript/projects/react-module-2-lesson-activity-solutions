//#region Task Specifications

// Create a Component:

// Create a React component named GuitarCard that represents a card displaying information about a guitar product.
// The component should have the following details:
// Guitar image
// Guitar model
// Guitar manufacturer
// Guitar body type
// Materials (neck, fretboard, body)
// Number of strings
// A button to view details

// Display a Component:

// Create a parent component, let's call it GuitarList.
// Inside GuitarList, render an instance of the Guitar component.
// Pass the provided guitar data as props to the Guitar component.

// Display Data within a Component:

// Pass the provided guitar data as props to the GuitarCard component.
// The data should include:
// Guitar image URL
// Guitar model
// Guitar manufacturer
// Guitar body type
// Materials (neck, fretboard, body)
// Number of strings
// Handle User Events:

// Add a button within the GuitarCard component, e.g., a "View Details" button.
// Implement an event handler function in the Guitar component that logs a message to the console when the button is clicked.
// The message should include the guitar's ID.
//#endregion

export default function Guitar() {
  const guitar = {
    id: "916E597F-C629-4306-BD31-BBF767D9A8AA",
    model: "Telecaster Thinline Original '60s",
    manufacturer: "Fender",
    bodyType: "Semi-Hollow",
    materials: {
      neck: "Maple",
      fretboard: "Maple (Coated)",
      body: "Mahogany",
    },
    strings: 6,
    image:
      "https://www.fmicassets.com/Damroot/ZoomJpg/10001/0110172834_gtr_frt_001_rr.jpg",
  };

  function onDetailsClick() {
    console.log(guitar);
  }

  return (
    <div className="guitar-card">
      <img src={guitar.image} alt={guitar.model} />
      <h2>{guitar.model}</h2>
      <p>Manufacturer: {guitar.manufacturer}</p>
      <p>Body Type: {guitar.bodyType}</p>
      <p>Materials:</p>
      <ul>
        <li>Neck: {guitar.materials.neck}</li>
        <li>Fretboard: {guitar.materials.fretboard}</li>
        <li>Body: {guitar.materials.body}</li>
      </ul>
      <p>Strings: {guitar.strings}</p>
      <button onClick={onDetailsClick}>View Details</button>
    </div>
  );
}
